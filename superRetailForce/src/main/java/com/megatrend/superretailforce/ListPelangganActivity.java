package com.megatrend.superretailforce;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.megatrend.superretailforce.adapter.ListPelangganAdapter;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;
import com.megatrend.superretailforce.model.Jenis;

import java.util.ArrayList;

public class ListPelangganActivity extends Activity {

    TextView titleTV;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ListView listPelangganLV;
    ArrayList<String> idList = new ArrayList<String>();
    ArrayList<String> nameList = new ArrayList<String>();
    ArrayList<String> levelHargaList = new ArrayList<String>();
    ArrayList<String> alamatList = new ArrayList<String>();
    ArrayList<String> kontakList = new ArrayList<String>();
    ArrayList<String> aktifList = new ArrayList<String>();
    ArrayList<Jenis> pelangganList = new ArrayList<Jenis>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_pelanggan);
        dataBase = DataBaseManager.instance();
        fm = new FontManager(this);
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        listPelangganLV = (ListView) findViewById(R.id.listPelangganLV);
        titleTV.setTypeface(medium);
        Cursor pelangganCsr = dataBase.selectPelangganIdName();
        if (pelangganCsr.getCount() > 0) {
            idList.clear();
            nameList.clear();
            levelHargaList.clear();
            pelangganList.clear();
            pelangganCsr.moveToFirst();
            while (pelangganCsr.isAfterLast() == false) {
                String id = pelangganCsr.getString(0);
                String Nama = pelangganCsr.getString(1);
                String levelHarga = pelangganCsr.getString(2);
                String Alamat = pelangganCsr.getString(3);
                String Kontak = pelangganCsr.getString(4);
                String Kontak2 = pelangganCsr.getString(5);
                String Aktif = pelangganCsr.getString(6);
                idList.add(id);
                nameList.add(Nama);
                levelHargaList.add(levelHarga);
                alamatList.add(Alamat);
                aktifList.add(Aktif);
                String isiKontak = "";
                if ((Kontak.equals("")) && (Kontak2.equals(""))) {
                    isiKontak = "-";
                } else if ((!Kontak.equals("")) && (!Kontak2.equals(""))) {
                    isiKontak = Kontak + " / " + Kontak2;
                } else if ((!Kontak.equals("")) && (Kontak2.equals(""))) {
                    isiKontak = Kontak;
                } else if ((Kontak.equals("")) && (!Kontak2.equals(""))) {
                    isiKontak = Kontak2;
                }
                kontakList.add(isiKontak);
                Jenis jens = new Jenis(id, Nama, levelHarga);
                pelangganList.add(jens);

                pelangganCsr.moveToNext();
            }
//			Collections.sort(pelangganList, Jenis.StuNameComparator);
            listPelangganLV.setAdapter(new ListPelangganAdapter(context, nameList, pelangganList));
            listPelangganLV.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    dataBase.deleteOrderTemp();
                    dataBase.deleteSalesOrder();
                    dataBase.deleteSyncTemp();
//					Intent intent = new Intent(getApplicationContext(), ListKategoriActivity.class);
//					intent.putExtra("idPelanggan", pelangganList.get(position).getId());
//					intent.putExtra("levelHarga", pelangganList.get(position).getCount());
//					startActivityForResult(intent, 1);
//					finish();

                    Intent intent = new Intent(getApplicationContext(), DashboardCustomerActivity.class);
                    intent.putExtra("idPelanggan", pelangganList.get(position).getId());
                    intent.putExtra("levelHarga", pelangganList.get(position).getCount());
                    intent.putExtra("alamat", alamatList.get(position));
                    intent.putExtra("kontak", kontakList.get(position));
                    intent.putExtra("aktif", aktifList.get(position));
                    intent.putExtra("nama", pelangganList.get(position).getNama());
                    startActivityForResult(intent, 1);
                    finish();
//					Toast.makeText(getApplicationContext(),pelangganList.get(position).getNama()+","+alamatList.get(position)+","+pelangganList.get(position).getCount(),Toast.LENGTH_LONG).show();
                }
            });

        } else {
            alert.showAlert("Tidak ada data ! Sync data terlebih dahulu !");
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 1) {
            if (resultCode == RESULT_OK) {
                finish();
            }
        }
    }
}
