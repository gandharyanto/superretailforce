package com.megatrend.superretailforce;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.os.Looper;
import android.support.v4.app.ActivityCompat;
import android.widget.TextView;
import android.widget.Toast;

import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class SplashActivity extends Activity {

    TextView splashTV;
    Typeface fonts;
    FontManager fm;
    DataBaseManager dataBase;
    ProgressDialogManager dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        String[] PERMISSIONS = {Manifest.permission.INTERNET,
                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.CAMERA};
        int PERMISSION_ALL = 1;
        if (!hasPermissions(this, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, PERMISSION_ALL);
        }
        dataBase = DataBaseManager.instance();
        dataBase.checkAndWriteTable();
        dialog = new ProgressDialogManager(this);
        splashTV = (TextView) findViewById(R.id.splashTV);
        fm = new FontManager(this);
        fonts = fm.getMediumTypeface();
        splashTV.setTypeface(fonts);
        Thread timer = new Thread() {
            @Override
            public void run() {
                long startTime = System.currentTimeMillis();
                long now = System.currentTimeMillis();
                if (now - startTime < 4000) {
                    try {
                        sleep(4000 - (now - startTime));
                    } catch (InterruptedException iEx) {

                    }
                }
                Cursor loginCheckedCsr = dataBase.selectAccount();
                Intent intent;
                if (loginCheckedCsr.getCount() > 0) {
                    loginCheckedCsr.moveToFirst();
                    String isLogin = loginCheckedCsr.getString(2);
                    if (isLogin.equals("1")) {
                        intent = new Intent(getApplicationContext(), MenuActivity.class);
                    } else {
                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                    }
                } else {
                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                }
                SplashActivity.this.finish();
                startActivity(intent);
            }
        };
        timer.start();
//        dialog.show();
//        getAccess();
    }

    public static boolean hasPermissions(Context context, String... permissions) {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public void getAccess() {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
                        HttpPost post = new HttpPost("http://www.aspirasi.co/srdonline/access.php");
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 20000);
                        JSONObject json = new JSONObject();
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                                    }
                                });
                            } else {
                                final JSONObject jsonArray = new JSONObject(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {

                                        try {
                                            String allowed = jsonArray.getString("allowed");
                                            if (allowed.equals("true")) {
                                                dialog.dismiss();
                                                Cursor loginCheckedCsr = dataBase.selectAccount();
                                                Intent intent;
                                                if (loginCheckedCsr.getCount() > 0) {
                                                    loginCheckedCsr.moveToFirst();
                                                    String isLogin = loginCheckedCsr.getString(2);
                                                    if (isLogin.equals("1")) {
                                                        intent = new Intent(getApplicationContext(), MenuActivity.class);
                                                    } else {
                                                        intent = new Intent(getApplicationContext(), LoginActivity.class);
                                                    }
                                                } else {
                                                    intent = new Intent(getApplicationContext(), LoginActivity.class);
                                                }
                                                SplashActivity.this.finish();
                                                startActivity(intent);
                                            } else {
                                                dialog.dismiss();
                                                Toast.makeText(getApplicationContext(), "You dant have access !!", Toast.LENGTH_LONG).show();
                                            }
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }


                                    }
                                });
                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (final Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Connection Error !!", Toast.LENGTH_LONG).show();
                }
            });
        }
    }

}
