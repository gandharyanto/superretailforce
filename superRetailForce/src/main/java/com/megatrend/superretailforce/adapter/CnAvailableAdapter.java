package com.megatrend.superretailforce.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.megatrend.superretailforce.R;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class CnAvailableAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> tipeList = new ArrayList<String>();
    ArrayList<String> noreturList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ArrayList<String> keteranganList = new ArrayList<String>();
    ArrayList<String> jumlahList = new ArrayList<String>();
    ArrayList<String> pakaiList = new ArrayList<String>();
    ArrayList<String> sisaList = new ArrayList<String>();
    ArrayList<String> salesidList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;
    double sum;
    ;
    boolean isSumcalc = true;

    public CnAvailableAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> tanggalList,
                              ArrayList<String> tipeList, ArrayList<String> noreturList, ArrayList<String> pelangganList
            , ArrayList<String> keteranganList, ArrayList<String> jumlahList, ArrayList<String> pakaiList
            , ArrayList<String> sisaList, ArrayList<String> salesidList) {
        context = c;
        this.nomorList = nomorList;
        this.tanggalList = tanggalList;
        this.tipeList = tipeList;
        this.noreturList = noreturList;
        this.pelangganList = pelangganList;
        this.keteranganList = keteranganList;
        this.jumlahList = jumlahList;
        this.pakaiList = pakaiList;
        this.sisaList = sisaList;
        this.salesidList = salesidList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
        sum = 0;
    }

    public int getCount() {

        return nomorList.size() + 1;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_ar_reminder_detail, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(regular);
        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTV);
        tanggalTV.setTypeface(regular);
        TextView jatuhTempoTV = (TextView) gridView.findViewById(R.id.jatuhTempoTV);
        jatuhTempoTV.setTypeface(regular);
        TextView umurTV = (TextView) gridView.findViewById(R.id.umurTV);
        umurTV.setTypeface(regular);
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.pelangganTV);
        pelangganTV.setTypeface(regular);
        TextView overTV = (TextView) gridView.findViewById(R.id.overTV);
        overTV.setTypeface(regular);
        TextView totalTV = (TextView) gridView.findViewById(R.id.totalTV);
        totalTV.setTypeface(regular);
        TextView bayarTV = (TextView) gridView.findViewById(R.id.bayarTV);
        bayarTV.setTypeface(regular);
        TextView potonganTV = (TextView) gridView.findViewById(R.id.potonganTV);
        potonganTV.setTypeface(regular);
        TextView sisaTV = (TextView) gridView.findViewById(R.id.sisaTV);
        sisaTV.setTypeface(regular);
        if (position < nomorList.size()) {
//					public CnAvailableAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> tanggalList,
//						ArrayList<String> tipeList, ArrayList<String> noreturList, ArrayList<String> pelangganList
//						, ArrayList<String> keteranganList, ArrayList<String> jumlahList, ArrayList<String> pakaiList
//						, ArrayList<String> sisaList, ArrayList<String> salesidList){
            nomorTV.setText(nomorList.get(position));

            tanggalTV.setText(setDate(tanggalList.get(position)));

            jatuhTempoTV.setText(tipeList.get(position));

            umurTV.setText(noreturList.get(position));

            overTV.setText(pelangganList.get(position));

            pelangganTV.setText(keteranganList.get(position));

            double totalDbl = Double.parseDouble(jumlahList.get(position));
            double totalDblTmp = Math.floor(totalDbl);
//    			totalTV.setText(jumlahList.get(position));
            totalTV.setText(formatter.format(totalDblTmp));

            double bayarDbl = Double.parseDouble(pakaiList.get(position));
            double bayarDblTmp = Math.floor(bayarDbl);
//    			bayarTV.setText(pakaiList.get(position));
            bayarTV.setText(formatter.format(bayarDblTmp));

            potonganTV.setText(sisaList.get(position));

            double sisaDbl = Double.parseDouble(sisaList.get(position));
            double sisaDblTmp = Math.floor(sisaDbl);
            if (isSumcalc == true) {
                sum = sum + sisaDblTmp;
            }

            sisaTV.setText(formatter.format(sisaDblTmp));
//    			sisaTV.setText(sisaList.get(position));
        } else {
            bayarTV.setTypeface(bold);
            sisaTV.setTypeface(bold);
            isSumcalc = false;
            bayarTV.setText("TOTAL");
            sisaTV.setText(formatter.format(sum));
        }
        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}