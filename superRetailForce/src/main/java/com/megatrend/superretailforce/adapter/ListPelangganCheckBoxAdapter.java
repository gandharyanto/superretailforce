package com.megatrend.superretailforce.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.megatrend.superretailforce.R;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import java.util.ArrayList;

public class ListPelangganCheckBoxAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nameList = new ArrayList<String>();

    public ListPelangganCheckBoxAdapter(Context c, ArrayList<String> nameList) {
        context = c;
        this.nameList = nameList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
    }

    public int getCount() {

        return nameList.size();
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView;

        gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_pelanggan_list, null);
        TextView nameTV = (TextView) gridView.findViewById(R.id.nameTV);
        nameTV.setTypeface(medium);
        nameTV.setText(nameList.get(position));
        return gridView;
    }
}