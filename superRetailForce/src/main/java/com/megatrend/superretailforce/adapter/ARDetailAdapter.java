package com.megatrend.superretailforce.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.megatrend.superretailforce.R;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ARDetailAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> keteranganList = new ArrayList<String>();
    ArrayList<String> debitList = new ArrayList<String>();
    ArrayList<String> kreditList = new ArrayList<String>();
    ArrayList<String> saldoList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;

    public ARDetailAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> tanggalList,
                           ArrayList<String> keteranganList, ArrayList<String> debitList, ArrayList<String> kreditList
            , ArrayList<String> saldoList) {
        context = c;
        this.nomorList = nomorList;
        this.tanggalList = tanggalList;
        this.keteranganList = keteranganList;
        this.debitList = debitList;
        this.kreditList = kreditList;
        this.saldoList = saldoList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return nomorList.size();
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_ar_detail, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(regular);
        nomorTV.setText(nomorList.get(position));

        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTV);
        tanggalTV.setTypeface(regular);
        tanggalTV.setText(setDate(tanggalList.get(position)));

        TextView keteranganTV = (TextView) gridView.findViewById(R.id.keteranganTV);
        keteranganTV.setTypeface(regular);
        keteranganTV.setText(keteranganList.get(position));

        double debitDbl = Double.parseDouble(debitList.get(position));
        TextView debitTV = (TextView) gridView.findViewById(R.id.debitTV);
        debitTV.setTypeface(regular);
        debitTV.setText(formatter.format(debitDbl));

        double kreditDbl = Double.parseDouble(kreditList.get(position));
        TextView kreditTV = (TextView) gridView.findViewById(R.id.kreditTV);
        kreditTV.setTypeface(regular);
        kreditTV.setText(formatter.format(kreditDbl));

        double saldoDbl = Double.parseDouble(saldoList.get(position));
        double saldoDblTmp = Math.floor(saldoDbl);
        TextView saldoTV = (TextView) gridView.findViewById(R.id.saldoTV);
        saldoTV.setTypeface(regular);
        saldoTV.setText(formatter.format(saldoDblTmp));

        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}