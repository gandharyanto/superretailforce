package com.megatrend.superretailforce.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.megatrend.superretailforce.R;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ReturnReportAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> kodeList = new ArrayList<String>();
    ArrayList<String> namaList = new ArrayList<String>();
    ArrayList<String> quantityList = new ArrayList<String>();
    ArrayList<String> satuanList = new ArrayList<String>();
    ArrayList<String> isiList = new ArrayList<String>();
    ArrayList<String> hargaList = new ArrayList<String>();
    ArrayList<String> subtotalList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    DecimalFormat formatter;
    double sum = 0;
    boolean isSum = false;
    DecimalFormatSymbols symbols;

    public ReturnReportAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> tanggalList,
                               ArrayList<String> kodeList, ArrayList<String> namaList, ArrayList<String> quantityList
            , ArrayList<String> satuanList, ArrayList<String> isiList, ArrayList<String> hargaList
            , ArrayList<String> subtotalList, ArrayList<String> totalList, ArrayList<String> pelangganList) {
        context = c;
        this.nomorList = nomorList;
        this.tanggalList = tanggalList;
        this.kodeList = kodeList;
        this.namaList = namaList;
        this.quantityList = quantityList;
        this.satuanList = satuanList;
        this.isiList = isiList;
        this.hargaList = hargaList;
        this.subtotalList = subtotalList;
        this.totalList = totalList;
        this.pelangganList = pelangganList;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return nomorList.size() + 1;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_return_report, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(bold);
        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTV);
        tanggalTV.setTypeface(regular);
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.pelangganTV);
        pelangganTV.setTypeface(regular);
        TextView jatuhTempoTV = (TextView) gridView.findViewById(R.id.kodeTV);
        jatuhTempoTV.setTypeface(regular);

        if (position < nomorList.size()) {
            nomorTV.setText(nomorList.get(position));


            tanggalTV.setText(setDate(tanggalList.get(position)));
            pelangganTV.setText(pelangganList.get(position));

            double sis1aDbl = Double.parseDouble(totalList.get(position));
            double ttl1DblTmp = Math.floor(sis1aDbl);
            jatuhTempoTV.setText(formatter.format(ttl1DblTmp));
            if (isSum == false) {
                sum = sum + ttl1DblTmp;
            }
        } else {
            tanggalTV.setTypeface(bold);
            jatuhTempoTV.setTypeface(bold);
            isSum = true;
            tanggalTV.setText("Grand Total");
            double ttl1DblTmp = Math.floor(sum);
            jatuhTempoTV.setText(formatter.format(ttl1DblTmp));
        }

        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}