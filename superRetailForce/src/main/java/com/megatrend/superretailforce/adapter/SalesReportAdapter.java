package com.megatrend.superretailforce.adapter;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.megatrend.superretailforce.R;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class SalesReportAdapter extends BaseAdapter {

    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    DecimalFormat formatter;
    DecimalFormatSymbols symbols;
    String grandTotal;

    public SalesReportAdapter(Context c, ArrayList<String> nomorList, ArrayList<String> tanggalList,
                              ArrayList<String> totalList, ArrayList<String> pelangganList, String grandTotal) {
        context = c;
        this.nomorList = nomorList;
        this.tanggalList = tanggalList;
        this.totalList = totalList;
        this.pelangganList = pelangganList;
        this.grandTotal = grandTotal;
        fm = new FontManager(c);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        formatter = (DecimalFormat) NumberFormat.getInstance(Locale.US);
        symbols = formatter.getDecimalFormatSymbols();
        symbols.setGroupingSeparator(',');
    }

    public int getCount() {

        return nomorList.size() + 1;
    }

    public Object getItem(int arg0) {

        return arg0;
    }

    public long getItemId(int arg0) {

        return arg0;
    }


    @Override
    public View getView(int position, View arg1, ViewGroup arg2) {


        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View gridView = new View(context);
        gridView = inflater.inflate(R.layout.model_sales_report, null);
        TextView nomorTV = (TextView) gridView.findViewById(R.id.nomorTV);
        nomorTV.setTypeface(bold);
        TextView tanggalTV = (TextView) gridView.findViewById(R.id.tanggalTV);
        tanggalTV.setTypeface(regular);
        TextView totalTV = (TextView) gridView.findViewById(R.id.totalTV);
        totalTV.setTypeface(regular);
        TextView pelangganTV = (TextView) gridView.findViewById(R.id.pelangganTV);
        pelangganTV.setTypeface(regular);

        try {
            if (position < nomorList.size()) {
                nomorTV.setText(nomorList.get(position));
                pelangganTV.setText(pelangganList.get(position));

                tanggalTV.setText(setDate(tanggalList.get(position)));


                double totalDbl = Double.parseDouble(totalList.get(position));
                double ttlDblTmp = Math.floor(totalDbl);

                totalTV.setText(formatter.format(ttlDblTmp));
            } else {
                totalTV.setTypeface(bold);
                tanggalTV.setTypeface(bold);
                tanggalTV.setText("Grand Total");


                double totalDbl = Double.parseDouble(grandTotal);
                double ttlDblTmp = Math.floor(totalDbl);

                totalTV.setText(formatter.format(ttlDblTmp));
            }
        } catch (Exception e) {
            Toast.makeText(context, e.toString(), Toast.LENGTH_LONG).show();
        }


        return gridView;
    }

    private String setDate(String dateString) {
        String result = "-";
        if (!dateString.equals("-") && !dateString.equals(" - ")) {
            SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
            Date date = null;
            try {
                date = fmt.parse(dateString);
            } catch (ParseException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }

            SimpleDateFormat fmtOut = new SimpleDateFormat("dd-MM-yyyy");
            result = fmtOut.format(date);
        }

        return result;
    }
}