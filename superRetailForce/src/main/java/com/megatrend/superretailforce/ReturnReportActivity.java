package com.megatrend.superretailforce;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;

import com.megatrend.superretailforce.adapter.ReturnReportAdapter;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import java.util.ArrayList;

public class ReturnReportActivity extends Activity {

    String id;
    String fromDate;
    String toDate;
    Typeface bold, regular, medium;
    FontManager fm;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    Context context;
    ArrayList<String> nomorList = new ArrayList<String>();
    ArrayList<String> tanggalList = new ArrayList<String>();
    ArrayList<String> kodeList = new ArrayList<String>();
    ArrayList<String> namaList = new ArrayList<String>();
    ArrayList<String> quantityList = new ArrayList<String>();
    ArrayList<String> satuanList = new ArrayList<String>();
    ArrayList<String> isiList = new ArrayList<String>();
    ArrayList<String> hargaList = new ArrayList<String>();
    ArrayList<String> subtotalList = new ArrayList<String>();
    ArrayList<String> totalList = new ArrayList<String>();
    ArrayList<String> pelangganList = new ArrayList<String>();
    ListView arLV;
    TextView titleTV, tv1, tv2, tv3, tv4, tv5, tv6, tv7, tv8, tv9, tv10, tv15;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_return_report);
        Bundle extras = getIntent().getExtras();
        id = extras.getString("id");
        fromDate = extras.getString("fromDate");
        toDate = extras.getString("toDate");
        dataBase = DataBaseManager.instance();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        context = this;
        fm = new FontManager(this);
        bold = fm.getBoldTypeface();
        arLV = (ListView) findViewById(R.id.arLV);
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        titleTV = (TextView) findViewById(R.id.titleTV);
        tv1 = (TextView) findViewById(R.id.tv1);
        tv2 = (TextView) findViewById(R.id.tv2);
        tv3 = (TextView) findViewById(R.id.tv3);
        tv4 = (TextView) findViewById(R.id.tv4);
        tv5 = (TextView) findViewById(R.id.tv5);
        tv6 = (TextView) findViewById(R.id.tv6);
        tv7 = (TextView) findViewById(R.id.tv7);
        tv8 = (TextView) findViewById(R.id.tv8);
        tv9 = (TextView) findViewById(R.id.tv9);
        tv10 = (TextView) findViewById(R.id.tv10);
        tv15 = (TextView) findViewById(R.id.tv15);
        titleTV.setTypeface(medium);
        tv1.setTypeface(bold);
        tv2.setTypeface(bold);
        tv3.setTypeface(bold);
        tv4.setTypeface(bold);
        tv5.setTypeface(bold);
        tv6.setTypeface(bold);
        tv7.setTypeface(bold);
        tv8.setTypeface(bold);
        tv9.setTypeface(bold);
        tv10.setTypeface(bold);
        tv15.setTypeface(bold);
        Cursor reminderCsr = dataBase.selectReturnReport(id, fromDate, toDate);
        if (reminderCsr.getCount() > 0) {
            nomorList.clear();
            tanggalList.clear();
            kodeList.clear();
            namaList.clear();
            quantityList.clear();
            satuanList.clear();
            isiList.clear();
            hargaList.clear();
            subtotalList.clear();
            totalList.clear();
            pelangganList.clear();
            reminderCsr.moveToFirst();
            while (reminderCsr.isAfterLast() == false) {
                String nomor = reminderCsr.getString(0);
                String tanggal = reminderCsr.getString(1);
                String kode = reminderCsr.getString(2);
                String nama = reminderCsr.getString(3);
                String quantity = reminderCsr.getString(4);
                String satuan = reminderCsr.getString(5);
                String isi = reminderCsr.getString(6);
                String harga = reminderCsr.getString(7);
                String subtotal = reminderCsr.getString(8);
                String total = reminderCsr.getString(9);
                String pelanggan = reminderCsr.getString(10);
                nomorList.add(nomor);
                tanggalList.add(tanggal);
                kodeList.add(kode);
                namaList.add(nama);
                quantityList.add(quantity);
                satuanList.add(satuan);
                isiList.add(isi);
                hargaList.add(harga);
                subtotalList.add(subtotal);
                totalList.add(total);
                pelangganList.add(pelanggan);
                reminderCsr.moveToNext();
            }
            arLV.setAdapter(new ReturnReportAdapter(context, nomorList, tanggalList, kodeList,
                    namaList, quantityList, satuanList, isiList, hargaList, subtotalList, totalList, pelangganList));
            arLV.setOnItemClickListener(new OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> parent, View view,
                                        int position, long id) {
                    if (position < nomorList.size()) {
                        Intent intent = new Intent(getApplicationContext(), ReturnReportDetailActivity.class);
                        intent.putExtra("Nomor", nomorList.get(position));
                        intent.putExtra("Pelanggan", pelangganList.get(position));
                        startActivity(intent);
                    }

                }
            });
        }
    }
}
