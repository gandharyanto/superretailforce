package com.megatrend.superretailforce;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.megatrend.superretailforce.helper.DataBaseManager;

public class InputPasswordActivity extends Activity {

    DataBaseManager dataBase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_input_password);
        dataBase = DataBaseManager.instance();
        TextView passwordTV = (TextView) findViewById(R.id.passwordTV);
        final EditText passwordET = (EditText) findViewById(R.id.passwordET);
        passwordTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                if (passwordET.getText().toString().length() > 0) {
                    Cursor csr = dataBase.selectAccount();
                    csr.moveToFirst();
                    String pass = csr.getString(1);
                    if (passwordET.getText().toString().equals(pass)) {
                        Intent returnIntent = new Intent();
                        setResult(RESULT_OK, returnIntent);
                        finish();
                    } else {
                        Toast.makeText(getApplicationContext(), "Wrong password !", Toast.LENGTH_LONG).show();
                    }
                }
            }
        });
    }
}
