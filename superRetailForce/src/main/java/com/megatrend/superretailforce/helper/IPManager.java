package com.megatrend.superretailforce.helper;

import android.database.Cursor;

public class IPManager {

    DataBaseManager dataBase;

    public IPManager() {
        dataBase = DataBaseManager.instance();
    }

    public String getIP() {
        Cursor ipCsr = dataBase.selectIP();
        ipCsr.moveToFirst();
        return ipCsr.getString(0);
    }
}
