package com.megatrend.superretailforce.helper;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.TextView;

import com.megatrend.superretailforce.R;

public class AlertDialogManager extends Dialog {

    Typeface font;
    FontManager fm;
    private TextView iv;

    public AlertDialogManager(Context context) {
        super(context, R.style.TransparentAlertDialog);

        WindowManager.LayoutParams wlmp = getWindow().getAttributes();
        wlmp.gravity = Gravity.CENTER_HORIZONTAL;
        getWindow().setAttributes(wlmp);
        setTitle(null);
        fm = new FontManager(context);
        setOnCancelListener(null);
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View alert = new View(context);
        alert = inflater.inflate(R.layout.alert, null);
        iv = (TextView) alert.findViewById(R.id.messageTV);
        font = fm.getMediumTypeface();
        iv.setTypeface(font);
        addContentView(alert, wlmp);
    }

    @Override
    public void show() {
        super.show();
    }

    ;

    public void showAlert(String message) {
        iv.setText(message);
        show();
    }
}