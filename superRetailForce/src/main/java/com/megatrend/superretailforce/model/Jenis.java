package com.megatrend.superretailforce.model;

import java.util.Comparator;

public class Jenis {

    public static Comparator<Jenis> StuNameComparator = new Comparator<Jenis>() {

        public int compare(Jenis s1, Jenis s2) {
            String StudentName1 = s1.getNama().toUpperCase();
            String StudentName2 = s2.getNama().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };
    String id;
    String Nama;
    String Count;

    public Jenis(String id, String Nama, String Count) {
        super();
        this.id = id;
        this.Nama = Nama;
        this.Count = Count;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getCount() {
        return Count;
    }

    public void setCount(String Count) {
        this.Count = Count;
    }
}
