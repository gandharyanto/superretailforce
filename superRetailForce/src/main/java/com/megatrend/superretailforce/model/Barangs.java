package com.megatrend.superretailforce.model;

import java.util.Comparator;

public class Barangs {

    public static Comparator<Barangs> StuNameComparator = new Comparator<Barangs>() {

        public int compare(Barangs s1, Barangs s2) {
            String StudentName1 = s1.getKode().toUpperCase();
            String StudentName2 = s2.getKode().toUpperCase();

            //ascending order
            return StudentName1.compareTo(StudentName2);

            //descending order
            //return StudentName2.compareTo(StudentName1);
        }
    };
    String id;
    String Nama;
    String Kode;
    boolean status;
    int Isi;

    public Barangs(String id, String Nama, boolean status, String Kode) {
        super();
        this.id = id;
        this.Nama = Nama;
        this.Kode = Kode;
        this.status = status;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNama() {
        return Nama;
    }

    public void setNama(String Nama) {
        this.Nama = Nama;
    }

    public String getKode() {
        return Kode;
    }

    public void setKode(String Kode) {
        this.Kode = Kode;
    }

    public boolean getStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

}
