package com.megatrend.superretailforce;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Looper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.TextView;

import com.megatrend.superretailforce.constant.Constant;
import com.megatrend.superretailforce.helper.AlertDialogManager;
import com.megatrend.superretailforce.helper.DataBaseManager;
import com.megatrend.superretailforce.helper.FontManager;
import com.megatrend.superretailforce.helper.IPManager;
import com.megatrend.superretailforce.helper.ProgressDialogManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.protocol.HTTP;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;


public class LoginActivity extends Activity {

    //	private static final String URL = "http://www.joanriffaldy.esy.es/srdonline";
//	private static final String LOGIN_URL = URL+"/user_login.php";
    Typeface bold, regular, medium;
    FontManager fm;
    TextView pleaseLoginTV, appNameTV, loginTV, changeIPTV;
    EditText usernameET, passwordET;
    AlertDialogManager alert;
    ProgressDialogManager dialog;
    DataBaseManager dataBase;
    IPManager ipManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        dataBase = DataBaseManager.instance();
        fm = new FontManager(this);
        ipManager = new IPManager();
        alert = new AlertDialogManager(this);
        dialog = new ProgressDialogManager(this);
        bold = fm.getBoldTypeface();
        regular = fm.getRegularTypeface();
        medium = fm.getMediumTypeface();
        changeIPTV = (TextView) findViewById(R.id.changeIPTV);
        pleaseLoginTV = (TextView) findViewById(R.id.pleaseLoginTV);
        appNameTV = (TextView) findViewById(R.id.appNameTV);
        loginTV = (TextView) findViewById(R.id.loginTV);
        appNameTV.setTypeface(medium);
        pleaseLoginTV.setTypeface(regular);
        usernameET = (EditText) findViewById(R.id.usernameET);
        passwordET = (EditText) findViewById(R.id.passwordET);
        loginTV.setTypeface(medium);
        usernameET.setTypeface(medium);
        passwordET.setTypeface(medium);

        Cursor ipCheck = dataBase.selectIP();
        GPSTracker gpsTracker = new GPSTracker(this);

        if (ipCheck.getCount() < 1) {
            ContentValues values = new ContentValues();
            values.put("name", "Default Server");
            values.put("ip", "http://103.200.7.58:8080/srdonline");
            values.put("aktif", "1");
            dataBase.insert("ip", values);
        }
        changeIPTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), ChangeIpActivity.class);
                startActivity(intent);
                finish();
            }
        });
        loginTV.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                if (usernameET.getText().toString().length() < 1) {
                    alert.showAlert("Please fill username !");
                } else if (usernameET.getText().toString().length() < 1) {
                    alert.showAlert("PLease fill password !");
                } else {
                    dialog.show();
                    loginCheck(usernameET.getText().toString(), passwordET.getText().toString());
                }
            }
        });
    }

    public void loginCheck(final String username, final String password) {
        try {
            Thread t = new Thread() {
                public void run() {
                    Looper.prepare();
                    try {
//                        HttpPost post = new HttpPost(ipManager.getIP() + Constant.LOGIN_URL);
                        HttpPost post = new HttpPost(Constant.NEW_BASE_URL + Constant.NEW_LOGIN_URL);
                        StringEntity se;
                        HttpResponse response;
                        HttpClient client = new DefaultHttpClient();
                        HttpConnectionParams.setConnectionTimeout(client.getParams(), 10000);
                        JSONObject json = new JSONObject();
                        json.put("Nama", username);
                        json.put("Password", password);
                        se = new StringEntity(json.toString());
                        se.setContentType(new BasicHeader(HTTP.CONTENT_TYPE, "application/json"));
                        post.setEntity(se);
                        if (post.isAborted()) {
                            runOnUiThread(new Runnable() {

                                @Override
                                public void run() {
                                    dialog.dismiss();
                                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                }
                            });

                        } else {

                            response = client.execute(post);
                            StringBuilder sb = new StringBuilder();
                            InputStream in = response.getEntity().getContent();
                            BufferedReader br = new BufferedReader(new InputStreamReader(in));
                            String line;
                            while ((line = br.readLine()) != null) {
                                sb.append(line);
                            }
                            String arrResult = sb.toString();
                            if (arrResult.equals("")) {
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();
                                        alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                                    }
                                });
                            } else {
                                final JSONObject jsonResult = new JSONObject(arrResult);
                                runOnUiThread(new Runnable() {

                                    @Override
                                    public void run() {
                                        dialog.dismiss();

                                        try {
                                            String idSalesman = jsonResult.getString("idSalesman");
//											String NamaSalesman = jsonResult.getString("NamaSalesman");
                                            if (idSalesman.equals("0")) {
                                                alert.showAlert("Username/ Password salah !");
                                            } else {
                                                dataBase.deleteAccount();
                                                ContentValues values = new ContentValues();
                                                values.put("username", username);
                                                values.put("password", password);
                                                values.put("isLogin", "1");
                                                values.put("idSalesman", idSalesman);
                                                values.put("NamaSalesman", username);
                                                dataBase.insert("account", values);
                                                Intent intent = new Intent(getApplicationContext(), MenuActivity.class);
                                                startActivity(intent);
                                                finish();
                                            }
                                        } catch (JSONException e) {
                                            // TODO Auto-generated catch block
                                            e.printStackTrace();
                                        }

                                    }
                                });


                            }

                        }
                    } catch (final Exception e) {
                        runOnUiThread(new Runnable() {

                            @Override
                            public void run() {
                                dialog.dismiss();
                                alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda." + e.getMessage());
                            }
                        });
                    }
                    Looper.loop();
                }
            };
            t.start();
        } catch (Exception e) {
            runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    dialog.dismiss();
                    alert.showAlert("Koneksi gagal! Mohon periksa koneksi internet anda.");
                }
            });
        }
    }
}
